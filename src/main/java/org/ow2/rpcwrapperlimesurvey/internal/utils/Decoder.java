/**
 This file is part of RPC LimeSurvey Wrapper.

 RPC LimeSurvey Wrapper is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 RPC LimeSurvey Wrapper is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with FusionDirectory (file COPYING).  If not, see <https://www.gnu.org/licenses/>.

 Copyright (C) 2019-2020 by Fraunhofer-Gesellschaft, All rights reserved
 */
package org.ow2.rpcwrapperlimesurvey.internal.utils;

import java.util.Arrays;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

import com.sun.org.apache.xml.internal.security.utils.Base64;

/**
 * Class for decoding strings.
 */
public class Decoder {
    /**
     * Decrypt a String
     * @param input the String to be decrypted.
     * @param md5 the key.
     * @return the decrypted String.
     */
    public static String decrypt(String input, String md5) {
        //System.out.println("Input for decoding is: " + input);
        try {
            byte[] decodedKey = Base64.decode(md5);
            Cipher cipher = Cipher.getInstance("AES");
            // rebuild key using SecretKeySpec
            SecretKey originalKey = new SecretKeySpec(Arrays.copyOf(decodedKey, 16), "AES");
            cipher.init(Cipher.DECRYPT_MODE, originalKey);
            byte[] cipherText = cipher.doFinal(Base64.decode(input));
            String decrypted=new String(cipherText);
            //System.out.println("Decrypted: " + decrypted);
            return decrypted;
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException(
                    "Error occured while decrypting data", e);
        }
    }

}