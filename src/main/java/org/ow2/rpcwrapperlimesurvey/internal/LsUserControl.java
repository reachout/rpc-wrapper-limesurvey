/**
 This file is part of RPC LimeSurvey Wrapper.

 RPC LimeSurvey Wrapper is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 RPC LimeSurvey Wrapper is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with FusionDirectory (file COPYING).  If not, see <https://www.gnu.org/licenses/>.

 Copyright (C) 2019-2020 by Fraunhofer-Gesellschaft, All rights reserved
 */
package org.ow2.rpcwrapperlimesurvey.internal;

import org.apache.http.client.CookieStore;
import org.apache.http.cookie.Cookie;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.cookie.BasicClientCookie;

import kong.unirest.Unirest;
import kong.unirest.UnirestInstance;

/**
 * Wrapper for LimeSurvey user control API
 */
public class LsUserControl {
  private final String YII_TOKEN = "YII_CSRF_TOKEN";

  private UnirestInstance unirest;
  private CookieStore cookieStore = new BasicCookieStore();
  private String host;
  private String username;
  private String password;

  public LsUserControl() {
    this.setup();
  }

  public LsUserControl(String host, String username, String password) {
    this.configure(host, username, password);
    this.setup();
  }

  public void configure(String host, String username, String password) {
    this.host = host;
    this.username = username;
    this.password = password;
  }

  /**
   * Configures the internal http Client.
   */
  private void setup() {
    CloseableHttpClient httpClient = HttpClients
      .custom()
      .setDefaultCookieStore(cookieStore)
      .disableRedirectHandling()
      .build();

    unirest = Unirest.spawnInstance();
    unirest.config().httpClient(httpClient)
      .setDefaultHeader("Connection", "keep-alive")
      .setDefaultHeader("Upgrade-Insecure-Requests", "1")
      .setDefaultHeader("Content-type", "application/x-www-form-urlencoded")
      .setDefaultHeader("User-Agent",
        "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36")
      .setDefaultHeader("Accept",
        "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3")
      .setDefaultHeader("Accept-Encoding", "gzip, deflate, br")
      .setDefaultHeader("Cache-Control", "max-age=0")
      .cookieSpec("standard-strict");
  }

  

  /**
   * @param userType User type (LDAP or db).
   * @param newUser Username.
   * @param newEmail EMail address.
   * @param newFullName Full name.
   * @throws LsException on creation error.
   * @see LsWrapperImplementation#createUser(String, String, String, String)
   */
  public void createUser(String userType, String newUser, String newEmail, String newFullName) throws LsException {
    // Create Yii CSRF token -> login using admin credentials + token -> store new token -> create user -> logout
    BasicClientCookie yiiCookie = (BasicClientCookie) this.createYiiToken();
    String yiiToken = yiiCookie.getValue().replace("%3D%3D", "==");
    this.login(yiiToken);
    cookieStore.addCookie(yiiCookie);
    
    int statusCode = unirest.post(this.host + "/index.php/admin/user/sa/adduser")
      .field("YII_CSRF_TOKEN", yiiToken)
      .field("user_type", userType)
      .field("new_user", newUser)
      .field("new_email", newEmail)
      .field("new_full_name", newFullName)
      .field("action", "adduser")
      .asEmpty().getStatus();
    
    if (statusCode != 302) {
      throw new LsException("Error (addUser): should respond with 302 but responded with: " + statusCode);
    }
    
    this.logout();
  }

  /**
   * @param userId user ID to delete.
   * @throws LsException on deletion error.
   * @see LsWrapperImplementation#deleteUser(int)
   */
  public void deleteUser(String userId) throws LsException {
    // Create Yii CSRF token -> login using admin credentials + token -> store new token -> create user -> logout
    BasicClientCookie yiiCookie = (BasicClientCookie) this.createYiiToken();
    String yiiToken = yiiCookie.getValue().replace("%3D%3D", "==");
    this.login(yiiToken);
    cookieStore.addCookie(yiiCookie);
    
    int statusCode = unirest.post(this.host + "/index.php/admin/user/sa/deluser")
      .field("YII_CSRF_TOKEN", yiiToken)
      .field("uid", userId)
      .field("action", "deluser")
      .asEmpty().getStatus();
    
    if (statusCode != 200) {
      throw new LsException("Error (deleteUser): should respond with 200 but responded with " + statusCode);
    }
    
    this.logout();
  }

  /**
   * @return A cookie containing a new Yii token.
   */
  private Cookie createYiiToken() {
    this.cookieStore.clear();
    unirest.get(this.host + "/index.php/admin/authentication/sa/login")
      .asEmpty();
    return getCookieByName(this.YII_TOKEN);
  }

  /**
   * Attempts to start a new LimeSurvey session. Required to make user management calls.
   * 
   * @param yiiToken An existing Yii CSRF token.
   */
  private void login(String yiiToken) throws LsException {
    int statusCode = unirest.post(this.host + "/index.php/admin/authentication/sa/login")
      .field("YII_CSRF_TOKEN", yiiToken)
      .field("authMethod", "AuthLDAP")
      .field("user", this.username)
      .field("password", this.password)
      .field("loginlang", "default")
      .field("action", "login")
      .field("width", "1920")
      .field("login_submit", "login")
      .asEmpty()
      .getStatus();
    if (statusCode != 302) {
      throw new LsException("Error (login): should respond with 302 but responded with: " + statusCode);
    }
  }

  /**
   * Ends session.
   */
  private void logout() {
    this.cookieStore.clear();
  }

  /**
   * @param Cookie The cookie name.
   * @return The first cookie with the given name.
   */
  private Cookie getCookieByName(String name) {
    for (Cookie cookie : cookieStore.getCookies()) {
      if (cookie.getName().equals(name)) {
        return cookie;
      }
    }
    return null;
  }
  
  public void shutdown() {
    unirest.shutDown();
  }
}