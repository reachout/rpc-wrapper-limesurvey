/**
 This file is part of RPC LimeSurvey Wrapper.

 RPC LimeSurvey Wrapper is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 RPC LimeSurvey Wrapper is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with FusionDirectory (file COPYING).  If not, see <https://www.gnu.org/licenses/>.

 Copyright (C) 2019-2020 by Fraunhofer-Gesellschaft, All rights reserved
 */

package org.ow2.rpcwrapperlimesurvey.internal;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

import javax.inject.Singleton;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import org.ow2.rpcwrapperlimesurvey.LsWrapper;
import org.xwiki.component.annotation.Component;

import kong.unirest.HttpResponse;
import kong.unirest.JacksonObjectMapper;
import kong.unirest.RequestBodyEntity;
import kong.unirest.Unirest;
import kong.unirest.UnirestException;

/**
 * Implementation of a <code>LsWrapper</code> component.
 */
@Component
@Singleton
public class LsWrapperImplementation implements LsWrapper {
  private final String rpcEndpointStandard = "/index.php/admin/remotecontrol";

  private LsUserControl lsUserControl;
  private URL url;
  private String lsUrl;
  private String username;
  private String password;
  private String sessionId;
  private String endpoint;
  private ObjectMapper mapper;

  /**
   * Initializes HTTP client as well as stores LimeSurvey credentials to enable calling RPC methods.
   * 
   * @param host the host of the LimeSurvey instance.
   * @param username Username.
   * @param password Password.
   */
  public void configure(String host, String username, String password) {
    try {
      // Utilizing dynamic parameters
      this.url = new URL(host);
      // LimeSurvey homepage
      this.lsUrl = this.url.getProtocol() + "://" + this.url.getAuthority();
    } catch (MalformedURLException e) {
      e.printStackTrace();
    }
    this.lsUserControl.configure(lsUrl, username, password);
    
    this.endpoint = this.rpcEndpointStandard;
    this.username = username;
    this.password = password;
  }

  public LsWrapperImplementation() {
    this.lsUserControl = new LsUserControl();
    this.setup();
  }

  private void setup() {
    this.configureHttpClient();
    mapper = new ObjectMapper();
  }

  @Override
  public Map<String, Object> login(String username, String password, String authMethod) {
    if (authMethod != "Authdb" && authMethod != "AuthLDAP") {
      return null;
    }
    ArrayNode params = mapper.createArrayNode().add(username).add(password).add(authMethod);
    HttpResponse<LsResponseBody> response = this.call(this.rpcEndpointStandard, "get_session_key", params);

    return LsResponseParser.parseToMap(response);
  }

  @Override
  public void logout() {
    if (this.sessionId == null || this.sessionId.isEmpty()) {
      return;
    }
    ArrayNode params = mapper.createArrayNode().add(this.sessionId);
    this.call("release_session_key", params);
    this.sessionId = null;
  }

  @Override
  public Map<String, Object> addSurvey(int surveyId, String surveyTitle, String surveyLanguage, String format) {
    ArrayNode params = mapper.createArrayNode().add(surveyId).add(surveyTitle).add(surveyLanguage).add(format);
    HttpResponse<LsResponseBody> response = this.callRoutine(this.rpcEndpointStandard, "add_survey", params);

    return LsResponseParser.parseToMap(response, (result) -> result.asInt(-1));
  }

  @Override
  public Map<String, Object> importSurvey(byte[] importData, String importDataType, String newSurveyName,
      int destSurveyId) {
    ArrayNode params = mapper.createArrayNode().add(importData).add(importDataType).add(newSurveyName)
        .add(destSurveyId);
    HttpResponse<LsResponseBody> response = this.callRoutine(this.rpcEndpointStandard, "import_survey", params);

    return LsResponseParser.parseToMap(response, (result) -> result.asInt(-1));
  }

  @Override
  public Map<String, Object> transferSurveyOwner(int surveyId, int newOwnerId) {
    final String propertyName = "owner_id";
    ObjectNode node = mapper.createObjectNode();
    node.put(propertyName, newOwnerId);

    JsonNode json = node;
    HttpResponse<LsResponseBody> response = this.setSurveyProperties(surveyId, json);
    return LsResponseParser.parseToMap(response,
        (result) -> result.has(propertyName) && result.get(propertyName).asBoolean() == true);
  }

  @Override
  public Map<String, Object> setSurveyProperties(int surveyId, String surveyData) {
    // Check if surveyData is a valid JSON string.
    try {
      JsonNode jsonSurveyData = mapper.readTree(surveyData);
      HttpResponse<LsResponseBody> response = this.setSurveyProperties(surveyId, jsonSurveyData);
      return LsResponseParser.parseToMap(response);
    } catch (IOException e) {
      // Return error output
      Map<String, Object> errorResult = new HashMap<>();
      errorResult.put("error", e.getMessage());
      errorResult.put("result", null);
      e.printStackTrace();
      return errorResult;
    }
  }

  /**
   * Calls <code>set_survey_properties</code>. Sets survey properties.
   * 
   * @param surveyId   The ID of the survey.
   * @param surveyData JSON object representing representing the survey property
   *                   changes.
   * @return
   */
  private HttpResponse<LsResponseBody> setSurveyProperties(int surveyId, JsonNode surveyData) {
    ArrayNode params = mapper.createArrayNode().add(surveyId).add(surveyData);
    HttpResponse<LsResponseBody> response = this.callRoutine(this.rpcEndpointStandard, "set_survey_properties", params);
    return response;
  }

  @Override
  public Map<String, Object> getSurveyProperties(int surveyId, String[] surveySettings) {
    ArrayNode settings = mapper.createArrayNode();
    for (String setting : surveySettings) {
      settings.add(setting);
    }
    ArrayNode params = mapper.createArrayNode().add(surveyId).add(settings);
    HttpResponse<LsResponseBody> response = this.callRoutine(this.rpcEndpointStandard, "get_survey_properties", params);

    return LsResponseParser.parseToMap(response);
  }

  @Override
  public Map<String, Object> setLanguageProperties(int surveyId, String surveyLocaleSettings, String lang) {
    // Check if surveyLocaleSettings is a valid JSON string.
    try {
      JsonNode jsonSurveyData = mapper.readTree(surveyLocaleSettings);
      ArrayNode params = mapper.createArrayNode().add(surveyId).add(jsonSurveyData).add(lang);
      HttpResponse<LsResponseBody> response = this.callRoutine(this.rpcEndpointStandard, "set_language_properties", params);
      return LsResponseParser.parseToMap(response);
    } catch (IOException e) {
      // Return error output
      Map<String, Object> errorResult = new HashMap<>();
      errorResult.put("error", e.getMessage());
      errorResult.put("result", null);
      e.printStackTrace();
      return errorResult;
    }
  }

  @Override
  public Map<String, Object> getLanguageProperties(int surveyId, String[] surveyLocaleSettings, String lang) {
    ArrayNode settings = mapper.createArrayNode();
    for (String setting : surveyLocaleSettings) {
      settings.add(setting);
    }
    ArrayNode params = mapper.createArrayNode().add(surveyId).add(settings).add(lang);
    HttpResponse<LsResponseBody> response = this.callRoutine(this.rpcEndpointStandard, "get_language_properties", params);

    return LsResponseParser.parseToMap(response);
  }


  @Override
  public Map<String, Object> getUserIdByUsername(String username) {
    // Use RPC method list_users to retrieve all users
    HttpResponse<LsResponseBody> response = this.listUsers();
    Map<String, Object> res = new HashMap<>();
    res.put("result", null);

    // Because this is a non-standard RPC method, we need to find the user ID manually
    try {
      JsonNode userList = LsResponseParser.parse(response);
      if (userList == null || !userList.isArray()) {
        res.put("error", "Error: malformed user list");
        return res;
      }
  
      // Search for user ID with with the given user name
      JsonNode foundUser = null;
      for (int i = 0; i < userList.size(); ++i) {
        JsonNode user = userList.get(i);
        if (user.get("users_name").textValue().equals(username)) {
          foundUser = user;
          break;
        }
      }
  
      if (foundUser == null) {
        res.put("error", "Error: user not found");
        return res;
      } else {
        int userId = foundUser.get("uid").asInt(-1);
        res.put("error", userId == -1 ? "Error: malformed user (has no uid property)" : null);
        res.put("result", userId);
        return res;
      }
    } catch (LsException e) {
      res.put("error", e.getCause());
      return res;
    }
  }

  /**
   * @return Calls <code>list_users</code>. Gets all users.
   */
  private HttpResponse<LsResponseBody> listUsers() {
    HttpResponse<LsResponseBody> response = this.callRoutine(this.rpcEndpointStandard, "list_users", mapper.createArrayNode());
    return response;
  }

  @Override
  public Map<String, Object> createUser(String userType, String newUser, String newEmail, String newFullName) {
    Map<String, Object> res = new HashMap<>();
    res.put("result", null);
    try {
      lsUserControl.createUser(userType, newUser, newEmail, newFullName);
      res.put("error", null);
      res.put("result", true);
    } catch (LsException e) {
      e.printStackTrace();
      res.put("error", e.getMessage());
    }
    return res;
  }

  @Override
  public Map<String, Object> deleteUser(int userId) {
    Map<String, Object> res = new HashMap<>();
    res.put("result", null);
    try {
      lsUserControl.deleteUser(Integer.toString(userId));
      res.put("error", null);
      res.put("result", true);
    } catch (LsException e) {
      e.printStackTrace();
      res.put("error", e.getMessage());
    }
    return res;
  }

  @Override
  public Map<String, Object> listSurveys(String username) {
    ArrayNode params = mapper.createArrayNode().add(username);
    HttpResponse<LsResponseBody> response = this.callRoutine(this.rpcEndpointStandard, "list_surveys", params);

    return LsResponseParser.parseToMap(response);
  }

  @Override
  public Map<String, Object> deleteSurvey(int surveyId) {
    ArrayNode params = mapper.createArrayNode().add(surveyId);
    HttpResponse<LsResponseBody> response = this.callRoutine(this.rpcEndpointStandard, "delete_survey", params);

    return LsResponseParser.parseToMap(response);
  }

  /**
   * Creates a new session, invokes an RPC method and ends the session.
   * 
   * This way, we create 'mini sessions' for each RPC method, ensuring that
   * session keys are valid.
   * 
   * @param endpoint The RPC endpoint.
   * @param method   The RPC method.
   * @param params   The RPC parameters as an array.
   * @return Response body.
   */
  private HttpResponse<LsResponseBody> callRoutine(String endpoint, String method, ArrayNode params) {
    Map<String, Object> loginResult = this.login(this.username, this.password, "AuthLDAP");

    String sessionId = (String) loginResult.get("result");
    params.insert(0, sessionId);
    HttpResponse<LsResponseBody> response = this.call(endpoint, method, params);

    this.logout();
    return response;
  }

  @Override
  public Map<String, Object> call(String method, String params) throws UnirestException {
    try {
      JsonNode jsonSurveyData = mapper.readTree(params);
      HttpResponse<LsResponseBody> response = this.call(method, (ArrayNode) jsonSurveyData);

      return LsResponseParser.parseToMap(response);
    } catch (IOException e) {
      // Return error output
      Map<String, Object> errorResult = new HashMap<>();
      errorResult.put("error", e.getMessage());
      errorResult.put("result", null);
      e.printStackTrace();
      return errorResult;
    }
  }

  @Override
  public HttpResponse<LsResponseBody> call(String method, ArrayNode params) throws UnirestException {
    return this.call(this.endpoint, method, params);
  }

  @Override
  public HttpResponse<LsResponseBody> call(String endpoint, String method, ArrayNode params) throws UnirestException {
    HttpResponse<LsResponseBody> response;
    response = this.doCall(endpoint, method, params).asObject(LsResponseBody.class);
    return response;
  }

  public void setEndpoint(String endpoint) {
    this.endpoint = endpoint;
  }

  /**
   * Executes call method and returns generic response object for further
   * processing.
   * 
   * @param endpoint Endpoint path, e.g. index.php/admin/remotecontrol for the
   *                 official RemoteControl endpoint.
   * @param method   Method name to call.
   * @param params   Method parameters.
   * @return RequestBodyEntity of the response.
   */
  private RequestBodyEntity doCall(String endpoint, String method, ArrayNode params) {
    RequestBodyEntity response;
    response = Unirest.post(this.lsUrl + endpoint).body(new LsRequestBody(method, params, 1));
    return response;
  }

  /**
   * Configures the internal HTTP client.
   */
  private void configureHttpClient() {
    // Only one time
    Unirest.config()
      .setObjectMapper(new JacksonObjectMapper())
      .setDefaultHeader("content-type", "application/json")
      .setDefaultHeader("connection", "keep-alive");
  }

  public void dispose() {
    if (this.lsUserControl != null) {
      this.lsUserControl.shutdown();
    }
    Unirest.shutDown();
  }
}