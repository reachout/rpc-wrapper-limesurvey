/**
 This file is part of RPC LimeSurvey Wrapper.

 RPC LimeSurvey Wrapper is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 RPC LimeSurvey Wrapper is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with FusionDirectory (file COPYING).  If not, see <https://www.gnu.org/licenses/>.

 Copyright (C) 2019-2020 by Fraunhofer-Gesellschaft, All rights reserved
 */
package org.ow2.rpcwrapperlimesurvey.internal;

public class LsException extends Exception {
  private static final long serialVersionUID = 4976287873246926837L;

  public LsException() {
    super();
  }

  public LsException(String msg) {
    super(msg);
  }

  public LsException (Throwable cause) {
    super(cause);
}

  public LsException(String message, Throwable cause) {
      super(message, cause);
  }
}