/**
 This file is part of RPC LimeSurvey Wrapper.

 RPC LimeSurvey Wrapper is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 RPC LimeSurvey Wrapper is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with FusionDirectory (file COPYING).  If not, see <https://www.gnu.org/licenses/>.

 Copyright (C) 2019-2020 by Fraunhofer-Gesellschaft, All rights reserved
 */
package org.ow2.rpcwrapperlimesurvey.internal;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

import com.fasterxml.jackson.databind.JsonNode;

import kong.unirest.HttpResponse;

public class LsResponseParser {
  /**
   * Attempts to parse LimeSurvey RPC response object.
   * 
   * @param response The LimeSurvey RPC response object.
   * @return The parsed response message
   * @throws LsException On faulty response object.
   */
  public static JsonNode parse(HttpResponse<LsResponseBody> response) throws LsException {
    if (!response.isSuccess()) {
      if (response.getParsingError().isPresent()) {
        throw new LsException(response.getParsingError().get().getMessage());
      } else {
        throw new LsException(response.getStatus() + ": " + response.getStatusText());
      }
    }

    LsResponseBody lsResponse = response.getBody();
    if (lsResponse == null) {
      throw new LsException("Error: response body is null");
    }

    if (lsResponse.error != null) {
      throw new LsException("Error:" + lsResponse.error);
    }

    JsonNode result = lsResponse.result;
    // throw when the result has a property "status" that is NOT equal to "OK".
    if (result.isObject() && result.get("status") != null && !result.get("status").textValue().equals("OK")) {
      throw new LsException(result.get("status").asText());
    } else {
      return result;
    }
  }

  public static Map<String, Object> parseToMap(HttpResponse<LsResponseBody> response) {
    return LsResponseParser.parseToMap(response,
        (result) -> result.isTextual() ? result.textValue() : result.toString());
  }

  /**
   * Attempts to parse a LimeSurvey RPC response object with respect to a defined
   * parsing functional to the result body and builds a result map.
   * 
   * @param response The LimeSurvey RPC response object.
   * @param parseFunctional The parsing functional that transforms the result before putting it into the result map.
   * @return A Map with two entries, 'error' and 'result'.
   */
  public static Map<String, Object> parseToMap(HttpResponse<LsResponseBody> response, Function<JsonNode, Object> parseFunctional) {
    Map<String, Object> map = new HashMap<String, Object>();
    try {
      JsonNode result = LsResponseParser.parse(response);
      map.put("result", parseFunctional.apply(result));
      map.put("error", null);
    } catch (LsException e) {
      map.put("error", e.getMessage());
      map.put("result", null);
    }
    return map;
  }
}