/**
 This file is part of RPC LimeSurvey Wrapper.

 RPC LimeSurvey Wrapper is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 RPC LimeSurvey Wrapper is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with FusionDirectory (file COPYING).  If not, see <https://www.gnu.org/licenses/>.

 Copyright (C) 2019-2020 by Fraunhofer-Gesellschaft, All rights reserved
 */

package org.ow2.rpcwrapperlimesurvey;

import java.util.Map;

import com.fasterxml.jackson.databind.node.ArrayNode;

import org.ow2.rpcwrapperlimesurvey.internal.LsResponseBody;
import org.xwiki.component.annotation.Role;

import kong.unirest.HttpResponse;
import kong.unirest.UnirestException;

/**
 * Interface (aka Role) of the Component.
 */
@Role
public interface LsWrapper {

  /**
   * Sets the default LimeSurvey RPC endpoint.
   * 
   * @param endpoint URL path of a LimeSurvey endpoint, e.g,
   *                 <code>index.php/admin/remotecontrol</code>.
   */
  public void setEndpoint(String endpoint);

  /**
   * Starts a new LimeSurvey RPC session.
   * 
   * Warning: do not call this function for other wrapper methods as they already
   * call `login()` and `logout()` for you. Instead, you want to call this
   * function when using the generic `call()` method.
   * 
   * @param username   Username.
   * @param password   Password.
   * @param authMethod Authentication method. Must be either <code>"Authdb"</code>
   *                   or <code>"AuthLDAP"</code>.
   * @return A {@link Map} that contains two keys: "error" and "result". "result"
   *         maps to the <code>int</code> session ID if login was successful and
   *         <code>null</code> if an error occured. "error" maps to
   *         <code>null</code>, if the method was successful and a string
   *         containing the corresponding error message, otherwise.
   */
  public Map<String, Object> login(String username, String password, String authMethod);

  /**
   * Ends existing LimeSurvey RPC session.
   */
  public void logout();

  /**
   * Calls <code>add_survey</code>. Adds an empty survey with minimum details.
   * 
   * <p>
   * Example:
   * </p>
   * 
   * <pre>
   * {@code
   *   Map<String, Object> res;
   *   res = ls.addSurvey(80808, "hello world", "en", "G");
   *   // (int) res["result"] == 80808 (if the specified id didn't exist before)
   * }
   * </pre>
   * 
   * @param surveyId       Desired id of the new survey. Set to "null" if an id
   *                       should be assigned automatically.
   * @param surveyTitle    Title of the new survey.
   * @param surveyLanguage Language code of the new survey, e.g., en for English.
   * @param format         Question appearance format (A, G or S) for "All on one
   *                       page", "Group by Group", "Single questions"
   * @return A {@link Map} that contains two keys: "error" and "result". "result"
   *         <code>int</code> id of the newly created survey and <code>null</code>
   *         if an error occured. "error" maps to <code>null</code>, if the method
   *         was successful and a string containing the corresponding error
   *         message, otherwise.
   */
  public Map<String, Object> addSurvey(int surveyId, String surveyTitle, String surveyLanguage, String format);

  /**
   * Calls <code>imports_survey</code>. Adds a new survey based on a template.
   * 
   * @param importData     Byte array containing data of a lss, csv, txt or survey
   *                       lsa archive. NOTE: do not encode the byte array in
   *                       base64. This function will handle the encoding for you.
   * @param importDataType lss, csv, txt or lsa.
   * @param newSurveyName  The optional new name of the survey.
   * @param destSurveyId   This is the new ID of the survey - if already used a
   *                       random one will be taken instead.
   * @return A {@link Map} that contains two keys: "error" and "result". "result"
   *         <code>int</code> id of the newly created survey and <code>null</code>
   *         if an error occured. "error" maps to <code>null</code>, if the method
   *         was successful and a string containing the corresponding error
   *         message, otherwise.
   */
  public Map<String, Object> importSurvey(byte[] importData, String importDataType, String newSurveyName,
      int destSurveyId);

  /**
   * Transfers ownership of an existing survey.
   * 
   * Calls <code>set_survey_properties</code>.
   * 
   * Note that this method is equivalent to
   * <code>setSurveyProperties(surveyId, {"owner_id":newOwnerId})</code>
   *
   * @param surveyId   The affected survey ID.
   * @param newOwnerId The new user ID to transfer ownership to. NOTE: the user ID
   *                   does not need to exist and will be set accordingly when
   *                   created later.
   * @return A {@link Map} that contains two keys: "error" and "result". "result"
   *         maps to <code>boolean</code> <code>true</code> if the transfer was
   *         successful and <code>null</code> if an error occured. "error" maps to
   *         <code>null</code>, if the method was successful and a string
   *         containing the corresponding error message, otherwise.
   */
  public Map<String, Object> transferSurveyOwner(int surveyId, int newOwnerId);

  /**
   * Sets properties of a survey.
   * 
   * Calls <code>set_survey_properties</code>.
   * 
   * <p>
   * Example:
   * </p>
   * 
   * <pre>
   * {@code
   *   Map<String, Object> res;
   *   res = ls.setSurveyProperties(42, "{\"sid\":\"399751\",\"owner_id\":\"39\"}");
   *   // res["result"].toString() == "{\"sid\":\"399751\",\"owner_id\":\"39\"}"
   * }
   * </pre>
   * 
   * @param surveyId   The survey ID.
   * @param surveyData A JSON string containing survey properties as a key with
   *                   their correspondent updated values, for example
   *                   <code>{"sid":"399751","owner_id":"39"}</code>
   * 
   * @return A {@link Map} that contains two keys: "error" and "result". "result"
   *         maps to the JSON string of every found property as a key with their
   *         updated values, for example
   *         <code>{"sid":"399751","owner_id":"39"}</code>. "error" maps to
   *         <code>null</code>, if the method was successful and a string
   *         containing the corresponding error message, otherwise.
   * @see <a href="https://api.limesurvey.org/classes/Survey.html">Survey</a>
   */
  public Map<String, Object> setSurveyProperties(int surveyId, String surveyData);

  /**
   * Get properties of a survey.
   *
   * @param surveyId       The survey ID.
   * @param surveySettings The array of the desired property names of the survey.
   *                       Leave the array empty to retrieve all properties.
   * 
   * @return A {@link Map} that contains two keys: "error" and "result". "result"
   *         maps to the JSON string of every found property as a key with their
   *         correspondent values, for example
   *         <code>{"sid":"399751","owner_id":"39"}</code>. "error" maps to
   *         <code>null</code>, if the method was successful and a string
   *         containing the corresponding error message, otherwise.
   * @see <a href="https://api.limesurvey.org/classes/Survey.html">Survey</a>
   */
  public Map<String, Object> getSurveyProperties(int surveyId, String[] surveySettings);

  /**
   * Get survey language properties.
   * 
   * @param surveyId             The survey ID.
   * @param surveyLocaleSettings A JSON string containing survey properties as a
   *                             key with their correspondent updated values, for
   *                             example <code>{"surveyls_title":"Test"}</code>
   * @param lang                 The survey language.
   * 
   * @return A {@link Map} that contains two keys: "error" and "result". "result"
   *         maps to the JSON string of every found property as a key with their
   *         updated values, for example <code>{"surveyls_title":"Test"}</code>.
   *         in case of success 'status' is 'OK', when save successful, otherwise
   *         error text. "error" maps to <code>null</code>, if the method was
   *         successful and a string containing the corresponding error message,
   *         otherwise.
   */
  public Map<String, Object> setLanguageProperties(int surveyId, String surveyLocaleSettings, String lang);

  /**
   * Get properties of a survey.
   *
   * @param surveyId             The survey ID.
   * @param surveyLocaleSettings The array of the desired property names of the
   *                             survey. Leave the array empty to retrieve all
   *                             properties.
   * @param lang                 The survey langauge.
   * 
   * @return A {@link Map} that contains two keys: "error" and "result". "result"
   *         maps to the JSON string of every found property as a key with their
   *         requested values, for example <code>{"surveyls_title":"Test"}</code>.
   *         "error" maps to <code>null</code>, if the method was successful and a
   *         string containing the corresponding error message, otherwise.
   */
  public Map<String, Object> getLanguageProperties(int surveyId, String[] surveyLocaleSettings, String lang);

  /**
   * Retrieves the first user id with its coresponding username.
   * 
   * Calls <code>list_users</code>.
   * 
   * @param username The username to search for.
   * @return A {@link Map} that contains two keys: "error" and "result". "result"
   *         <code>int</code> found user id and <code>null</code> if an error
   *         occured. "error" maps to <code>null</code>, if the method was
   *         successful and a string containing the corresponding error message,
   *         otherwise.
   */
  public Map<String, Object> getUserIdByUsername(String username);

  /**
   * Creates a new admin user in LimeSurvey.
   *
   * @param userType    The authentication type. Use "DB" for interal database.
   *                    and "LDAP" for LDAP authentication.
   * @param newUser     Username of the new user.
   * @param newEmail    Email of the new user.
   * @param newFullName Full name of new user.
   * @return A {@link Map} that contains two keys: "error" and "result". "result"
   *         maps <code>boolean</code> <code>true</code> if user creation was
   *         successful and <code>null</code> if an error occured. "error" maps to
   *         <code>null</code>, if the method was successful and a string
   *         containing the corresponding error message, otherwise.
   */
  public Map<String, Object> createUser(String userType, String newUser, String newEmail, String newFullName);

  /**
   * Deletes a user in LimeSurvey.
   *
   * @param userId user ID of the user to be deleted.
   * @return A {@link Map} that contains two keys: "error" and "result". "result"
   *         maps <code>boolean</code> <code>true</code> if user deletion was
   *         successful and <code>null</code> if an error occured. "error" maps to
   *         <code>null</code>, if the method was successful and a string
   *         containing the corresponding error message, otherwise.
   */
  public Map<String, Object> deleteUser(int userId);

  /**
   * List all surveys belonging to a survey.
   * 
   * Only admins are authorized to list surveys of every user. Otherwise, will
   * return surveys belonging of the user.
   * 
   * @param username Username to get list of surveys. Leave it as
   *                 <code>null</code> when to get surveys belonging to the
   *                 caller.
   * @return A {@link Map} that contains two keys: "error" and "result". "result"
   *         maps <code>String</code> JSON array of surveys containing the
   *         following keys:
   *         <ul>
   *         <li><code>sid</code> the ID of the survey
   *         <li><code>surveyls_title</code> the title of the survey
   *         <li><code>startdate</code> start date
   *         <li><code>expires</code> expiration date
   *         <li><code>active</code> if survey is active <code>Y</code> or
   *         <code>N</code>
   *         </ul>
   *         , if the method was successful and a string containing the
   *         corresponding error message, otherwise.
   */
  public Map<String, Object> listSurveys(String username);

  /**
   * Calls <code>delete_survey</code>. Deletes a survey.
   * 
   * @param surveyId the survey ID.
   * @return A {@link Map} that contains two keys: "error" and "result". "result"
   *         <code>int</code> of OK status if removal was successful. "error" maps
   *         to <code>null</code>, if the method was successful and a string
   *         containing the corresponding error message, otherwise.
   */
  public Map<String, Object> deleteSurvey(int surveyId);

  /**
   * Generic LimeSurvey RPC call method for users to the currently set endpoint.
   * 
   * Note that this method is not part of session management as callers have to
   * provide their session key manually in params.
   * 
   * 
   * @param method Method name to call.
   * @param params Method parameters. Must be a JSON array string.
   * @return A {@link Map} that contains two keys: "error" and "result". "result"
   *         maps <code>String</code> of the result if the method call was
   *         successful. "error" maps to <code>null</code>, if the method was
   *         successful and a string containing the corresponding error message,
   *         otherwise.
   * @see #call(String, String, ArrayNode)
   * @throws UnirestException on HTTP errors.
   */
  public Map<String, Object> call(String method, String params) throws UnirestException;

  /**
   * Generic LimeSurvey RPC call method to the currently set endpoint.
   * 
   * @param method Method name to call.
   * @param params Method parameters.
   * @return Response body.
   * @see #call(String, String, ArrayNode)
   * @throws UnirestException on HTTP errors.
   */
  public HttpResponse<LsResponseBody> call(String method, ArrayNode params) throws UnirestException;

  /**
   * Generic LimeSurvey RPC call method.
   * 
   * @param endpoint LimeSurvey endpoint.
   * @param method   Method name to call.
   * @param params   Method parameters.
   * @return Response body.
   * @throws UnirestException on HTTP errors.
   */
  public HttpResponse<LsResponseBody> call(String endpoint, String method, ArrayNode params) throws UnirestException;
}
