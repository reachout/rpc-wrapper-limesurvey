/**
 This file is part of RPC LimeSurvey Wrapper.

 RPC LimeSurvey Wrapper is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 RPC LimeSurvey Wrapper is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with FusionDirectory (file COPYING).  If not, see <https://www.gnu.org/licenses/>.

 Copyright (C) 2019-2020 by Fraunhofer-Gesellschaft, All rights reserved
 */

package org.ow2.rpcwrapperlimesurvey.script;

import java.io.File;
import java.io.FileInputStream;
import java.util.Map;
import java.util.Properties;

import javax.inject.Named;
import javax.inject.Singleton;

import org.apache.commons.codec.digest.DigestUtils;
import org.ow2.rpcwrapperlimesurvey.internal.LsWrapperImplementation;
import org.ow2.rpcwrapperlimesurvey.internal.utils.Decoder;
import org.xwiki.component.annotation.Component;
import org.xwiki.component.manager.ComponentLifecycleException;
import org.xwiki.component.phase.Disposable;
import org.xwiki.component.phase.Initializable;
import org.xwiki.component.phase.InitializationException;
import org.xwiki.script.service.ScriptService;

/**
 * Make the LimeSurvey API available to scripting.
 */
@Component
@Named("limesurvey")
@Singleton
public class LsWrapperScriptService implements ScriptService, Initializable, Disposable {
  private LsWrapperImplementation ls;

  @Override
  public void initialize() throws InitializationException {
    System.out.println("[LsWrapper] ScriptService initialized");
    ls = new LsWrapperImplementation();
  }

  private String decryptPassword(String password) {
    com.sun.org.apache.xml.internal.security.Init.init();
    try {
      FileInputStream is = new FileInputStream(new File("/etc/ow2/wrappers/wrappers.conf"));
      Properties propertiesResource = new Properties();
      propertiesResource.load(is);
      String keyFileLocation = (String) propertiesResource.get("KEYFILE_LOCATION");
      FileInputStream keyfileio = new FileInputStream(new File(keyFileLocation));
      String md5 = DigestUtils.md5Hex(keyfileio);
      String decryptedPassword = Decoder.decrypt(password, md5);
      return decryptedPassword;
    } catch (Exception e) {
      e.printStackTrace();
      return null;
    }
  }

  public void init(String host, String adminUsername, String encryptedAdminPassword) {
    if (host == null) {
      System.out.println("[LsWrapper] host is null");
    }
    if (adminUsername == null) {
      System.out.println("[LsWrapper] username is null");
    }
    if (encryptedAdminPassword == null) {
      System.out.println("[LsWrapper] password is null");
    }

    String decryptedPassword = this.decryptPassword(encryptedAdminPassword);

    if (decryptedPassword == null) {
      System.out.println("[LsWrapper] decrypted password is null");
      return;
    }

    this.doInit(host, adminUsername, decryptedPassword);
  }

  public void doInit(String host, String adminUsername, String adminPassword) {
    this.ls.configure(host, adminUsername, adminPassword);
    System.out.println("[LsWrapper] Initialized");
  }

  public Map<String, Object> login(String username, String password) {
    return this.ls.login(username, password, "AuthLDAP");
  }

  public void logout() {
    this.ls.logout();
  }

  public Map<String, Object> addSurvey(int surveyId, String surveyTitle, String surveyLanguage, String format) {
    return this.ls.addSurvey(surveyId, surveyTitle, surveyLanguage, format);
  }

  public Map<String, Object> importSurvey(byte[] importData, String importDataType, String newSurveyName,
      int destSurveyId) {
    return this.ls.importSurvey(importData, importDataType, newSurveyName, destSurveyId);
  }

  public Map<String, Object> transferSurveyOwner(int surveyId, int newOwnerId) {
    return this.ls.transferSurveyOwner(surveyId, newOwnerId);
  }

  public Map<String, Object> setSurveyProperties(int surveyId, String surveySettings) {
    return this.ls.setSurveyProperties(surveyId, surveySettings);
  }

  public Map<String, Object> getSurveyProperties(int surveyId, String[] surveySettings) {
    return this.ls.getSurveyProperties(surveyId, surveySettings);
  }

  public Map<String, Object> setLanguageProperties(int surveyId, String surveyLocaleSettings, String lang) {
    return this.ls.setLanguageProperties(surveyId, surveyLocaleSettings, lang);
  }

  public Map<String, Object> getLanguageProperties(int surveyId, String[] surveyLocaleSettings, String lang) {
    return this.ls.getLanguageProperties(surveyId, surveyLocaleSettings, lang);
  }

  public Map<String, Object> getUserIdByUsername(String username) {
    return this.ls.getUserIdByUsername(username);
  }

  public Map<String, Object> createUser(String userType, String newUser, String newEmail, String newFullName) {
    return this.ls.createUser(userType, newUser, newEmail, newFullName);
  }

  public Map<String, Object> deleteUser(int userId) {
    return this.ls.deleteUser(userId);
  }

  public Map<String, Object> listSurveys(String username) {
    return this.ls.listSurveys(username);
  }

  public Map<String, Object> deleteSurvey(int surveyId) {
    return this.ls.deleteSurvey(surveyId);
  };

  public void setEndpoint(String endpoint) {
    this.ls.setEndpoint(endpoint);
  }

  public Map<String, Object> call(String method, String params) {
    return this.ls.call(method, params);
  }

  @Override
  public void dispose() throws ComponentLifecycleException {
    this.ls.dispose();
    this.ls = null;
    System.out.println("[LsWrapper] ScriptService disposed");
  }

}
