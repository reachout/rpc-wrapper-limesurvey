/**
 This file is part of RPC LimeSurvey Wrapper.

 RPC LimeSurvey Wrapper is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 RPC LimeSurvey Wrapper is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with FusionDirectory (file COPYING).  If not, see <https://www.gnu.org/licenses/>.

 Copyright (C) 2019-2020 by Fraunhofer-Gesellschaft, All rights reserved
 */

package org.ow2.rpcwrapperlimesurvey;

import static com.github.tomakehurst.wiremock.client.WireMock.*;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import com.github.tomakehurst.wiremock.junit.WireMockRule;

import org.ow2.rpcwrapperlimesurvey.LsWrapper;
import org.ow2.rpcwrapperlimesurvey.internal.LsWrapperImplementation;
import org.junit.After;
import org.junit.Assert;
import org.junit.ClassRule;
import org.junit.Test;
import org.xwiki.test.annotation.AllComponents;

/**
 * Tests for the {@link LsWrapper} component.
 */
@AllComponents
public class LoginTest {

  private String admin;
  private String adminPwd;

  private Properties propertiesResource;

  private LsWrapperImplementation ls;

  @ClassRule
  public static WireMockRule wireMockRule = new WireMockRule(9080);

  /**
   * Loads admin credentials.
   * 
   * @throws Exception Throws if admin credentials could not be retrieved.
   */
  {
    // Settings file with superadmin credentials
    // should be located in src/test/resources/settings.conf
    System.out.println("Startup - retrieving admin credentials");
    InputStream is = getClass().getResourceAsStream("/settings.conf");
    propertiesResource = new Properties();
    try {
      propertiesResource.load(is);
    } catch (IOException e) {
      e.printStackTrace();
    }
    admin = (String) propertiesResource.get("LDAP_ADMIN");
    adminPwd = (String) propertiesResource.get("LDAP_ADMINPASSWORD");
    System.out.println("Startup - retrieval successful");
    ls = new LsWrapperImplementation();
    ls.configure("http://localhost:9080", admin, adminPwd);
  }

  @Test
  public void testLoginSuccess() throws Exception {
    stubFor(post(urlEqualTo("/index.php/admin/remotecontrol"))
            .willReturn(aResponse()
                .withStatus(200)
                .withHeader("Content-Type", "application/json")
                .withBody("{\"id\":1,\"result\":\"6htqat38fyr4v7iu72nqgv7xgavkvfcz\",\"error\":null}")));
    String sessionId = ls.login(admin, adminPwd, "AuthLDAP").get("result").toString();

    verify(postRequestedFor(urlEqualTo("/index.php/admin/remotecontrol"))
        .withRequestBody(containing("{\"method\":\"get_session_key\",\"params\":[\"" + admin + "\",\"" + adminPwd + "\",\"AuthLDAP\"],\"id\":1}")));

    Assert.assertTrue(!sessionId.isEmpty());
    Assert.assertEquals("6htqat38fyr4v7iu72nqgv7xgavkvfcz", sessionId);
  }

  @Test
  public void testLoginFail() throws Exception {
    stubFor(post(urlEqualTo("/index.php/admin/remotecontrol"))
            .willReturn(aResponse()
                .withStatus(200)
                .withHeader("Content-Type", "application/json")
                .withBody("{\"id\":1,\"result\":{\"status\":\"Invalid user name or password\"},\"error\":null}")));
    
    System.out.println(admin);
    System.out.println(adminPwd);
    String errorMessage = ls.login(admin, adminPwd, "AuthLDAP").get("error").toString();
    Assert.assertEquals("Invalid user name or password", errorMessage );
  }

  @After
  public void after() throws Exception {
    this.ls.dispose();
  }
}
