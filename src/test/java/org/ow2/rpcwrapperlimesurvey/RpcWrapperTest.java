/**
 This file is part of RPC LimeSurvey Wrapper.

 RPC LimeSurvey Wrapper is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 RPC LimeSurvey Wrapper is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with FusionDirectory (file COPYING).  If not, see <https://www.gnu.org/licenses/>.

 Copyright (C) 2019-2020 by Fraunhofer-Gesellschaft, All rights reserved
 */

package org.ow2.rpcwrapperlimesurvey;

import static com.github.tomakehurst.wiremock.client.WireMock.aResponse;
import static com.github.tomakehurst.wiremock.client.WireMock.containing;
import static com.github.tomakehurst.wiremock.client.WireMock.get;
import static com.github.tomakehurst.wiremock.client.WireMock.post;
import static com.github.tomakehurst.wiremock.client.WireMock.postRequestedFor;
import static com.github.tomakehurst.wiremock.client.WireMock.stubFor;
import static com.github.tomakehurst.wiremock.client.WireMock.urlEqualTo;
import static com.github.tomakehurst.wiremock.client.WireMock.verify;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.is;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import com.github.tomakehurst.wiremock.junit.WireMockRule;

import org.apache.commons.io.IOUtils;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Test;
import org.ow2.rpcwrapperlimesurvey.internal.LsWrapperImplementation;

/**
 * Tests for the {@link LsWrapper} component.
 */
public class RpcWrapperTest {
  private final String TOKEN = "6htqat38fyr4v7iu72nqgv7xgavkvfcz";

  private String admin;
  private String adminPwd;
  private Properties propertiesResource;

  private LsWrapperImplementation ls;

  @ClassRule
  public static WireMockRule wireMockRule = new WireMockRule(9081);

  {
    // Settings file with superadmin credentials
    // should be located in src/test/resources/settings.conf
    InputStream is = getClass().getResourceAsStream("/settings.conf");
    propertiesResource = new Properties();
    try {
      propertiesResource.load(is);
    } catch (IOException e) {
      e.printStackTrace();
    }
    admin = (String) propertiesResource.get("LDAP_ADMIN");
    adminPwd = (String) propertiesResource.get("LDAP_ADMINPASSWORD");
    ls = new LsWrapperImplementation();
    ls.configure("http://localhost:9081", admin, adminPwd);
    // Use something like this to quickly test the real service
    // ls = new LsWrapperImplementation("https://limesurvey.dev.ow2.org", admin, adminPwd);
  }

  @Before
  public void init() throws Exception {
    // Assume login and logout are correct
    stubFor(post(urlEqualTo("/index.php/admin/remotecontrol"))
      .withRequestBody(containing("{\"method\":\"get_session_key\""))
      .willReturn(aResponse()
        .withStatus(200)
        .withHeader("Content-Type", "application/json")
        .withBody("{\"id\":1,\"result\":\"" + TOKEN + "\",\"error\":null}")));
   
    stubFor(post(urlEqualTo("/index.php/admin/remotecontrol"))
      .withRequestBody(containing("{\"method\":\"release_session_key\""))
      .willReturn(aResponse()
        .withStatus(200)
        .withHeader("Content-Type", "application/json")
        .withBody("{\"id\":1,\"result\":\"null\",\"error\":null}")));
  }

  @Test
  public void testAddSurvey() throws Exception {
    final int SURVEY_ID = 123456;
    stubFor(post(urlEqualTo("/index.php/admin/remotecontrol"))
      .withRequestBody(containing("{\"method\":\"add_survey\""))
      .willReturn(aResponse()
        .withStatus(200)
        .withHeader("Content-Type", "application/json")
        .withBody("{\"id\":1,\"result\":\"" + SURVEY_ID + "\",\"error\":null}")));
    int result = (int) ls.addSurvey(SURVEY_ID, "hello world", "en", "G").get("result");
    Assert.assertEquals(SURVEY_ID, result);
  }

  @Test
  public void testImportSurvey() throws Exception {
    final int SURVEY_ID = 123456;
    stubFor(post(urlEqualTo("/index.php/admin/remotecontrol"))
    .withRequestBody(containing("{\"method\":\"import_survey\""))
    .willReturn(aResponse()
      .withStatus(200)
      .withHeader("Content-Type", "application/json")
      .withBody("{\"id\":1,\"result\":\"" + SURVEY_ID + "\",\"error\":null}")));

    InputStream is = RpcWrapperTest.class.getResourceAsStream("/SurveyTemplate.lss");
    byte[] bytes = IOUtils.toByteArray(is);

    String result = ls.importSurvey(bytes, "lss", "Imported wrapper survey", 1234).get("result").toString();
    Assert.assertEquals(SURVEY_ID, Integer.parseInt(result));
  }

  @Test
  public void testSetSurveyProperties() throws Exception {
    final int SURVEY_ID = 123456;
    final String PROPERTIES = "{\"owner_id\":39}";
    stubFor(post(urlEqualTo("/index.php/admin/remotecontrol"))
      .withRequestBody(containing("{\"method\":\"set_survey_properties\""))
      .willReturn(aResponse()
        .withStatus(200)
        .withHeader("Content-Type", "application/json")
        .withBody("{\"id\":1,\"result\":{\"owner_id\":\"39\"},\"error\":null}")));
      
    String result = (String) ls.setSurveyProperties(SURVEY_ID, PROPERTIES).get("result");

    verify(postRequestedFor(urlEqualTo("/index.php/admin/remotecontrol"))
        .withRequestBody(containing("{\"method\":\"set_survey_properties\",\"params\":[\""+ TOKEN +"\",123456,{\"owner_id\":39}],\"id\":1}")));

    Assert.assertEquals("{\"owner_id\":\"39\"}", result);
  }

  @Test
  public void testGetSurveyProperties() throws Exception {
    final int SURVEY_ID = 123456;
    final String[] PROPERTIES = new String[]{"sid","owner_id","minh"};
    stubFor(post(urlEqualTo("/index.php/admin/remotecontrol"))
      .withRequestBody(containing("{\"method\":\"get_survey_properties\""))
      .willReturn(aResponse()
        .withStatus(200)
        .withHeader("Content-Type", "application/json")
        .withBody("{\"id\":1,\"result\":{\"sid\":\"" + SURVEY_ID + "\",\"owner_id\":\"39\"},\"error\":null}")));
      
    String result = (String) ls.getSurveyProperties(SURVEY_ID, PROPERTIES).get("result");

    verify(postRequestedFor(urlEqualTo("/index.php/admin/remotecontrol"))
        .withRequestBody(containing("{\"method\":\"get_survey_properties\",\"params\":[\""+ TOKEN +"\",123456,[\"sid\",\"owner_id\",\"minh\"]],\"id\":1}")));

    Assert.assertEquals("{\"sid\":\"" + SURVEY_ID + "\",\"owner_id\":\"39\"}", result);
  }

  @Test
  public void testSetLanguageProperties() throws Exception {
    final int SURVEY_ID = 123456;
    final String PROPERTIES = "{\"surveyls_title\":\"Test\",\"surveyls_url\":\"https://google.com\"}";
    final String LANG = "en";
    stubFor(post(urlEqualTo("/index.php/admin/remotecontrol"))
      .withRequestBody(containing("{\"method\":\"set_language_properties\""))
      .willReturn(aResponse()
        .withStatus(200)
        .withHeader("Content-Type", "application/json")
        .withBody("{\"id\":1,\"result\":{\"surveyls_title\":true,\"surveyls_url\":true,\"status\":\"OK\"},\"error\":null}")));
      
    String result = (String) ls.setLanguageProperties(SURVEY_ID, PROPERTIES, LANG).get("result");

    verify(postRequestedFor(urlEqualTo("/index.php/admin/remotecontrol"))
        .withRequestBody(containing("{\"method\":\"set_language_properties\",\"params\":[\"" + TOKEN + "\"," + SURVEY_ID + "," + PROPERTIES + ",\"" + LANG + "\"],\"id\":1}")));

    Assert.assertEquals("{\"surveyls_title\":true,\"surveyls_url\":true,\"status\":\"OK\"}", result);
  }

  @Test
  public void testGetLanguageProperties() throws Exception {
    final int SURVEY_ID = 123456;
    final String[] PROPERTIES = new String[]{"surveyls_title","surveyls_url"};
    final String LANG = "en";
    stubFor(post(urlEqualTo("/index.php/admin/remotecontrol"))
      .withRequestBody(containing("{\"method\":\"get_language_properties\""))
      .willReturn(aResponse()
        .withStatus(200)
        .withHeader("Content-Type", "application/json")
        .withBody("{\"id\":1,\"result\":{\"surveyls_title\":\"test\",\"surveyls_url\":\"https://reachout-project.eu\"},\"error\":null}")));
      
    String result = (String) ls.getLanguageProperties(SURVEY_ID, PROPERTIES, LANG).get("result");

    verify(postRequestedFor(urlEqualTo("/index.php/admin/remotecontrol"))
        .withRequestBody(containing("{\"method\":\"get_language_properties\",\"params\":[\""+ TOKEN +"\"," + SURVEY_ID + ",[\"surveyls_title\",\"surveyls_url\"],\"" + LANG + "\"],\"id\":1}")));

    Assert.assertEquals("{\"surveyls_title\":\"test\",\"surveyls_url\":\"https://reachout-project.eu\"}", result);
  }

  @Test
  public void transferSurveyOwner() throws Exception {
    final int SURVEY_ID = 1234;
    final int UID = 39;
    stubFor(post(urlEqualTo("/index.php/admin/remotecontrol"))
    .withRequestBody(containing("{\"method\":\"set_survey_properties\""))
    .willReturn(aResponse()
      .withStatus(200)
      .withHeader("Content-Type", "application/json")
      .withBody("{\"id\":1,\"result\":{\"owner_id\":true},\"error\":null}")));

    boolean result = (boolean) ls.transferSurveyOwner(SURVEY_ID, UID).get("result");
    Assert.assertTrue(result);
  }

  @Test
  public void getUserIdByUsername() throws Exception {
    InputStream is = RpcWrapperTest.class.getResourceAsStream("/userList.json");
    String responseBody = IOUtils.toString(is, "UTF8");
    stubFor(post(urlEqualTo("/index.php/admin/remotecontrol"))
    .withRequestBody(containing("{\"method\":\"list_users\""))
    .willReturn(aResponse()
      .withStatus(200)
      .withHeader("Content-Type", "application/json")
      .withBody(responseBody)));

    int result = (int) ls.getUserIdByUsername("dmv").get("result");
    Assert.assertEquals(5, result);
    result = (int) ls.getUserIdByUsername("wrapperuser6").get("result");
    Assert.assertEquals(39, result);
  }

  @Test
  public void testAddUser() throws Exception {
    final String phpSessionId = "d92ad9i89ndf5rn3btm0h7sm35";
    final String yiiToken = "aXRxWVR4cGVrM3RSN2NQMEJOcDNIV2ZLSDduUGxDMDM7KUEmWO9U-gzfhpa-Nok5QHtp7mC9vkrv9YoMn6PkgQ%3D%3D";

    stubFor(get(urlEqualTo("/index.php/admin/authentication/sa/login"))
    .willReturn(aResponse()
      .withStatus(200)
      .withHeader("Set-Cookie", "PHPSESSID=" + phpSessionId + "; path=/; secure; HttpOnly")
      .withHeader("Set-Cookie", "YII_CSRF_TOKEN=" + yiiToken + "; path=/; secure")));

    stubFor(post(urlEqualTo("/index.php/admin/authentication/sa/login"))
    .withRequestBody(containing("YII_CSRF_TOKEN=" + yiiToken))
    .withRequestBody(containing("authMethod=AuthLDAP"))
    .withRequestBody(containing("user=" + this.admin))
    .withRequestBody(containing("password=" + this.adminPwd))
    .willReturn(aResponse()
      .withStatus(302)
      .withHeader("Set-Cookie", "PHPSESSID=0r8crmfrgnm4o7d6fmh8nl6p15; path=/; secure; HttpOnly")
      .withHeader("Set-Cookie", "YII_CSRF_TOKEN=deleted; expires=Thu, 01-Jan-1970 00:00:01 GMT; Max-Age=0; path=/")));

    stubFor(post(urlEqualTo("/index.php/admin/user/sa/adduser"))
    .withRequestBody(containing("YII_CSRF_TOKEN=" + yiiToken))
    .withRequestBody(containing("user_type=DB"))
    .withRequestBody(containing("new_user=wrapperuser8"))
    .withRequestBody(containing("new_email=test%40wrapper.com"))
    .withRequestBody(containing("new_full_name=Wrapper+User"))
    .withRequestBody(containing("action=adduser"))
    .willReturn(aResponse()
      .withStatus(302)));

    boolean isCreated = (boolean) ls.createUser("DB", "wrapperuser8", "test@wrapper.com", "Wrapper User").get("result");    
    Assert.assertTrue(isCreated);
  }

  @Test
  public void testDeleteUser() throws Exception {
    final String phpSessionId = "d92ad9i89ndf5rn3btm0h7sm35";
    final String yiiToken = "aXRxWVR4cGVrM3RSN2NQMEJOcDNIV2ZLSDduUGxDMDM7KUEmWO9U-gzfhpa-Nok5QHtp7mC9vkrv9YoMn6PkgQ%3D%3D";

    stubFor(get(urlEqualTo("/index.php/admin/authentication/sa/login"))
    .willReturn(aResponse()
      .withStatus(200)
      .withHeader("Set-Cookie", "PHPSESSID=" + phpSessionId + "; path=/; secure; HttpOnly")
      .withHeader("Set-Cookie", "YII_CSRF_TOKEN=" + yiiToken + "; path=/; secure")));

    stubFor(post(urlEqualTo("/index.php/admin/authentication/sa/login"))
    .withRequestBody(containing("YII_CSRF_TOKEN=" + yiiToken))
    .withRequestBody(containing("authMethod=AuthLDAP"))
    .withRequestBody(containing("user=" + this.admin))
    .withRequestBody(containing("password=" + this.adminPwd))
    .willReturn(aResponse()
      .withStatus(302)
      .withHeader("Set-Cookie", "PHPSESSID=0r8crmfrgnm4o7d6fmh8nl6p15; path=/; secure; HttpOnly")
      .withHeader("Set-Cookie", "YII_CSRF_TOKEN=deleted; expires=Thu, 01-Jan-1970 00:00:01 GMT; Max-Age=0; path=/")));

    stubFor(post(urlEqualTo("/index.php/admin/user/sa/deluser"))
    .withRequestBody(containing("YII_CSRF_TOKEN=" + yiiToken))
    .withRequestBody(containing("uid=89"))
    .withRequestBody(containing("action=deluser"))
    .willReturn(aResponse()
      .withStatus(200)));

    boolean isCreated = (boolean) ls.deleteUser(89).get("result");    
    Assert.assertTrue(isCreated);
  }

  @Test
  public void testListSurveys() throws Exception {
    final String USERNAME = null;
    InputStream is = RpcWrapperTest.class.getResourceAsStream("/surveyList.json");
    String responseBody = IOUtils.toString(is, "UTF8");
    stubFor(post(urlEqualTo("/index.php/admin/remotecontrol"))
      .withRequestBody(containing("{\"method\":\"list_surveys\""))
      .willReturn(aResponse()
        .withStatus(200)
        .withHeader("Content-Type", "application/json")
        .withBody(responseBody)));
      
    String result = (String) ls.listSurveys(USERNAME).get("result");

    verify(postRequestedFor(urlEqualTo("/index.php/admin/remotecontrol"))
        .withRequestBody(containing("{\"method\":\"list_surveys\",\"params\":[\"" + TOKEN + "\",null],\"id\":1}")));

    assertThat(result, containsString("{\"sid\":\"398222\",\"surveyls_title\":\"Majid Test jeromey\",\"startdate\":\"2019-03-12 17:54:00\",\"expires\":\"2022-01-01 00:00:00\",\"active\":\"N\"}"));
  }

  @Test
  public void testDeleteSurvey() throws Exception {
    final int SURVEY_ID = 123456;
    stubFor(post(urlEqualTo("/index.php/admin/remotecontrol"))
      .withRequestBody(containing("{\"method\":\"delete_survey\""))
      .willReturn(aResponse()
        .withStatus(200)
        .withHeader("Content-Type", "application/json")
        .withBody("{\"id\":1,\"result\":{\"status\":\"OK\"},\"error\":null}")));
    String result = (String) ls.deleteSurvey(SURVEY_ID).get("result");
    Assert.assertEquals("{\"status\":\"OK\"}", result);
  }

  @Test
  public void testCall() throws Exception {
    // Simulate addSurvey
    final String SURVEY_ID = "123456";
    stubFor(post(urlEqualTo("/index.php/admin/remotecontrol"))
      .withRequestBody(containing("{\"method\":\"add_survey\""))
      .willReturn(aResponse()
        .withStatus(200)
        .withHeader("Content-Type", "application/json")
        .withBody("{\"id\":1,\"result\":\"" + SURVEY_ID + "\",\"error\":null}")));
    
    String result = ls.call("add_survey", "[\"" + TOKEN + "\",\"hello world\",\"en\",\"G\"]").get("result").toString();
    
    verify(postRequestedFor(urlEqualTo("/index.php/admin/remotecontrol"))
        .withRequestBody(containing("{\"method\":\"add_survey\",\"params\":[\"" + TOKEN + "\",\"hello world\",\"en\",\"G\"],\"id\":1}")));
    
    Assert.assertThat(SURVEY_ID, is(result));
  }

  @After
  public void after() throws Exception {
    this.ls.dispose();
  }


}
