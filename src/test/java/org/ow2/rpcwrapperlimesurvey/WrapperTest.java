/**
 This file is part of RPC LimeSurvey Wrapper.

 RPC LimeSurvey Wrapper is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 RPC LimeSurvey Wrapper is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with FusionDirectory (file COPYING).  If not, see <https://www.gnu.org/licenses/>.

 Copyright (C) 2019-2020 by Fraunhofer-Gesellschaft, All rights reserved
 */
package org.ow2.rpcwrapperlimesurvey;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.ow2.rpcwrapperlimesurvey.internal.LsWrapperImplementation;
import org.xwiki.test.mockito.MockitoComponentMockingRule;

public class WrapperTest {
    @Rule
    public final MockitoComponentMockingRule<LsWrapperImplementation> mocker = new MockitoComponentMockingRule<>(
            LsWrapperImplementation.class);

    @Before
    public void setUp() throws Exception {

    }

    @Test
    public void hello() throws Exception {
        assertEquals("Should return 76", true, true);
    }
}