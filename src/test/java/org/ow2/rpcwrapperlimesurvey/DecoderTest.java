/**
 This file is part of RPC LimeSurvey Wrapper.

 RPC LimeSurvey Wrapper is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 RPC LimeSurvey Wrapper is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with FusionDirectory (file COPYING).  If not, see <https://www.gnu.org/licenses/>.

 Copyright (C) 2019-2020 by Fraunhofer-Gesellschaft, All rights reserved
 */
package org.ow2.rpcwrapperlimesurvey;

import static org.junit.Assert.assertEquals;

import java.io.InputStream;

import org.apache.commons.codec.digest.DigestUtils;
import org.junit.Test;
import org.ow2.rpcwrapperlimesurvey.internal.utils.Decoder;

public class DecoderTest {

    @Test
    public void testDecode() throws Exception {
        InputStream keyfileio = getClass().getResourceAsStream("/key.txt");
        String md5 = DigestUtils.md5Hex(keyfileio);
        String decryptedPassword = Decoder.decrypt("bEO86XSjCH3kE00Dt5xIGA==", md5);
        assertEquals("You Win !", decryptedPassword);
    }
}