This is the repository for the JSON-RPC Limesurvey wrapper extension for XWiki
=======
# RPC LimeSurvey Wrapper

A Java LimeSurvey RPC client as an XWiki ScriptService with additional utility methods.

## Documentation

A generated javadoc static page can be found [here](https://dmv.ow2.io/rpc-wrapper-limesurvey-doc/). The full overview of the API can be found at the [LsWrapperImplementation.java](https://dmv.ow2.io/rpc-wrapper-limesurvey-doc/org/ow2/rpcwrapperlimesurvey/internal/LsWrapperImplementation.html) javadoc page.

## Getting started

The prerequisites are

- Java Development Kit JDK8 or higher
- Apache Maven

### Installation

Run `mvn package` in the root of the project folder. This will build a `rpcwrapperlimesurvey-x.x.xx-SNAPSHOT.jar` in `target` ready for usage as an XWiki ScriptService.

### Deployment

Deployment requires authorization. You need to put your valid OW2 credentials into Maven's `settings.xml` file like here:

```
<settings>
  <servers>
    <server>
      <id>ow2.snapshot</id>
      <username>USERNAME</username>
      <password>PASS</password>
    </server>
  </servers>
</settings>
```

There are two locations where a settings.xml file may live:

- The Maven install: `${maven.home}/conf/settings.xml`
- A user’s install: `${user.home}/.m2/settings.xml`

For more information, we refer to the [settings reference](https://maven.apache.org/settings.html).

Once done, the deployment process can be started. After bumping the product version in `pom.xml`, run `mvn deploy`. This will deploy the binary to the XWiki repository specified inside `pom.xml`. Once done, head to the XWiki admin page, go to `'Extensions'->'Advanced search'`. Search for the id `org.ow2:rpcwrapperlimesurvey` with the current product version specified in `pom.xml`. Remove this extension and install the new deployed version. **Important:** do not attempt to install the new version by updating, this will somehow break XWiki. Please reinstall the extension by removing and installing.

## Generating documentation

Use

```
mvn javadoc:javadoc
```

## Usage (Velocity)

This section covers the commonly used API methods. Please visit the full [documentation](https://dmv.ow2.io/rpc-wrapper-limesurvey-doc/) for more details.

### Initialize the API

```
$services.limesurvey.init("https://limesurvey.dev.ow2.org","admin","adminpassword")
```

Initializes the API with `https://limesurvey.dev.ow2.org` as the host and `admin:adminpassword` as the admin credentials. Please do not use a host without the protocol like `limesurvey.dev.ow2.org`.

Note that this will **not** throw an error when parameters are faulty, like wrong credentials or wrong URL. Instead, all API methods will return errors.

### Adding a survey

```
#set($result=$services.limesurvey.addSurvey(1234, "My survey", "en", "G"))
$result
```

will result in `{result=1234, error=null}` on successful call (new survey id would be `1234`, but might be a random assigned integer when already taken). An example for a faulty call would be `{result=null, error=Invalid session key}` when user credentials using `Init()` were incorrect.

### Deleting a survey

```
#set($result=$services.limesurvey.deleteSurvey(1234))
$result
```

will result in `{result="OK", error=null}` on successful call.

### Importing a survey

```
#set($result=$services.limesurvey.importSurvey($campaigndata.getBytes(),"lss", "My imprted survey", "1234"))
$result
```

will result in `{result=1234, error=null}` on success.

### Get survey information

To get a specific information of a survey:

```
#set($result=$services.limesurvey.getSurveyProperties(12345, ["sid","owner_id"]))
$result
```

would result `{result={"sid":"12345","owner_id":"1"}, error=null}`

If you want to retrieve all properties of a survey, input an empty array `[]` as parameter:

```
#set($result=$services.limesurvey.getSurveyProperties(12345, []))
$result
```

```
{result={"sid":"12345","owner_id":"1","gsid":"1","admin":"Vo Duy Minh","active":"N","expires":null,"startdate":null,..., error=null}
```

### Set survey information

```
#set($result=$services.limesurvey.setSurveyProperties(12345, "{\"owner_id\":39}"))
$result
```

results in would result `{result={"owner_id":"39"}, error=null}` with the corresponding updated properties.

### Get survey language property

To get a specific information of a survey:

```
#set($result=$services.limesurvey.getLanguageProperties(12345, ["surveyls_title","surveyls_url"],"en"))
$result
```

would result `{result={"surveyls_title":"Another Minnie Campaign 2","surveyls_url":"https:\/\/reachout-project.eu"}, error=null}`

If you want to retrieve all language properties of a survey, input an empty array `[]` as parameter.

Instead of using `"en"` as language parameter, you can input `"null"` to retrieve the default language properties.

### Set survey language property

```
#set($result=$services.limesurvey.setLanguageProperties(12345, '{"surveyls_title":"Test","surveyls_url":"https:\/\/google.com"}','en'))
$result
```

results in would result `{result={"surveyls_title":true,"surveyls_url":true,"status":"OK"}, error=null}` with the corresponding updated properties.

### Transfer survey owner

(Note: this function is equivalent to `setSurveyProperties` setting `owner_id`.)

```
#set($result=$services.limesurvey.transferSurveyOwner(233318, 46))))
$result
```

results in `{result=true, error=null}` when transfer was successful.

### Get user ID by username

```
#set($result=$services.limesurvey.getUserIdByUsername("dmv"))
$result
```

results in `{result=5, error=null}` with `result` representing the user ID.

### Generic call method

In cases where methods are not implemented yet, you can use this generic call method to do anything within the LimeSurvey documentation:

**Important:** this is a low-level method that is not part of session key management. Users need to provide their own session key (e.g. from `login()`) as specified in the LimeSurvey RemoteControl documentation.

```
#set($result=$services.limesurvey.call('add_survey', '["6htqat38fyr4v7iu72nqgv7xgavkvfcz","hello world","en","G"]'))
$result
```

*(Notice the brackets '[]' surrounding the second parameter as they are required because you must specify a valid JSON array as string.)*

results in `{result=1234, error=null}` (the newly added survey id).
### Non-RPC methods

This implements methods that do not conform to the official LimeSurvey RPC documentation. Use with caution.

#### Create user

Using internal database:

```
$services.limesurvey.createUser('DB', 'wrapperuser8', 'test@wrapper.com', 'Wrapper User')
```

Using LDAP:

```
$services.limesurvey.createUser('LDAP', 'ldapUser', '', '')
```

#### Delete user

Attempts to delete a user using their user ID:

```
$services.limesurvey.deleteUser(89)
```
## License

 RPC LimeSurvey Wrapper is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 RPC LimeSurvey Wrapper is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with LimeSurvey (file COPYING).  If not, see <https://www.gnu.org/licenses/>.

 Copyright © 2019-2020 by Fraunhofer-Gesellschaft, All rights reserved
